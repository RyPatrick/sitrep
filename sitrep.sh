#!/bin/bash
# Scan LAN for online hosts


IP=$(ip address | grep wlp | grep inet | awk '{print $2}' | cut -d "/" -f 1)
CIDR=$(ip address | grep wlp | grep inet | awk '{print $2}' | cut -d "/" -f 2)
OPTIONS="-sP"

nmap $OPTIONS $IP/$CIDR
